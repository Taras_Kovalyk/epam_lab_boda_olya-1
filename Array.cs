using System;
namespace test
{
	class Array
	{
		private int[] _array;
		public int BeginIndex { get; set; }

		public Array(int size)
		{
			_array = new int[size];
			BeginIndex = 0;
		}

		public Array(int size, int beginIndex) : this(size)
		{
			BeginIndex = beginIndex;
		}

		public int this[int index]
		{
			

			get
			{
				var temp = int.MinValue;
				
				try
				{
					temp = _array[index - BeginIndex];
				}
				catch (IndexOutOfRangeException)
				{
					Console.WriteLine("Incorect number of element. Array doesn't have element {0}", index);
				}
				return temp;
			}
			set
			{
				try
				{
					_array[index - BeginIndex] = value;
				}
				catch (IndexOutOfRangeException)
				{
					Console.WriteLine("Incorect number of element. Array doesn't have element {0}", index);
				}
				
			}		
			
		}

		public int Length
		{
			get
			{
				return _array.Length;
			}
		}

		public override bool Equals(object obj)
		{
			Array array = obj as Array;
			
			if ((array == null) || (array.Length!= this.Length) || (array.BeginIndex != this.BeginIndex))
			{
				return false;
			} 
			
			for (var i = array.BeginIndex; i < array.Length+ array.BeginIndex; i++)
			{
				if (array[i] != this[i])
				{
					return false;
				}
			}
				return true;	
		}
		
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		
		public static bool operator ==(Array array1, Array array2)
		{
			return Equals(array1, array2);
		}
		
		public static bool operator !=(Array array1, Array array2)
		{
			return !(Equals(array1, array2));
		}
		
		public static bool operator >(Array array1, Array array2)
		{
			if ((array1.Length!= array2.Length) || (array1.BeginIndex != array2.BeginIndex))
			{
				return false;
			}
			
			var index = array1.BeginIndex;
			var length = array1.Length;
			
			for (var i = index; i < index + length; i++)
			{
				if (array1[i] <= array2[i])
				{
					return false;
				}
			}
			
			return true;
		}
		
		public static bool operator <(Array array1, Array array2)
		{
			if ((array1.Length!= array2.Length) || (array1.BeginIndex != array2.BeginIndex))
			{
				return false;
			}
			
			var index = array1.BeginIndex;
			var length = array1.Length;
			
			for (var i = index; i < index + length; i++)
			{
				if (array1[i] >= array2[i])
				{
					return false;
				}
			}
			
			return true;
		}
		
		public static bool operator >=(Array array1, Array array2)
		{
			if ((array1.Length!= array2.Length) || (array1.BeginIndex != array2.BeginIndex))
			{
				return false;
			}
			
			var index = array1.BeginIndex;
			var length = array1.Length;
			
			for (var i = index; i < index + length; i++)
			{
				if (array1[i] < array2[i])
				{
					return false;
				}
			}
			
			return true;
		}
		
		public static bool operator <=(Array array1, Array array2)
		{
			if ((array1.Length != array2.Length) || (array1.BeginIndex != array2.BeginIndex))
			{
				return false;
			}
			
			var index = array1.BeginIndex;
			var length = array1.Length;
			
			for (var i = index; i < index + length; i++)
			{
				if (array1[i] != array2[i])
				{
					return false;
				}
			}
			
			return true;
		}
			public static Array Add(Array array1, Array array2)
		{
			if (array1 == null || array2 == null)
			{
				throw new ArgumentNullException("Methog get null argument");
			}
			var length1 = array1.Length;
			var length2 = array2.Length;
			var index1 = array1.BeginIndex;	
			var index2 = array2.BeginIndex;		
			
			var result = new Array(length1, index1);

			if ((length1 == length2) && (index1 == index2))
			{

				for (int i = index1; i < index1 + length1; i++)
				{
					result[i] = array1[i] + array2[i];
				}
				
				return result;
			}
			else
			{
				throw new Exception("Error: different size of array or begin index.");
			}
				
		}
		
		public static Array Subtract(Array array1, Array array2)
		{
			if (array1 == null || array2 == null)
			{
				throw new ArgumentNullException("Methog get null argument");
			}
			
			var length1 = array1.Length;
			var length2 = array2.Length;
			var index1 = array1.BeginIndex;	
			var index2 = array2.BeginIndex;		
			
			var result = new Array(length1, index1);

			if ((length1 == length2) && (index1 == index2))
			{

				for (int i = index1; i < index1 + length1; i++)
				{
					result[i] = array1[i] - array2[i];
				}
				
				return result;
			}
			else
			{
				throw new Exception("Error: different size of array or begin index.");
			}
				
		}
		public static Array Multiply(Array array, int number)
		{
			if (array == null)
			{
				throw new ArgumentNullException("Methog get null argument");
			}
			
			var length = array.Length;
			var index = array.BeginIndex;
			
			var result = new Array(length, index);
			
			for (int i = index; i < length + index; i++)
			{
				result[i] = array[i] * number; 
			}
			
			return result;
		}
		public void Fill(int rand)
		{
			Random rnd = new Random();
			for (int i = this.BeginIndex; i < this.BeginIndex + this.Length; i++)
			{
				this[i] = rnd.Next(rand++);
			}
		}
		public void Print()
		{
			for (int i = this.BeginIndex; i < this.BeginIndex + this.Length; i++)
			{
				Console.Write("{0}\t", this[i]);
			}
			Console.WriteLine();
		}
	}

}